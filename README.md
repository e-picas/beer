BEER - The most famous (and useful) unix command
================================================

Introducing the famous `beer` command which tells you the way from you next beer time ...

-   to use it, run `./beer`
-   to read its manpage, run `man ./beer.man`
-   to install it in your system, run `./install.sh `

Any contribution is welcome!
