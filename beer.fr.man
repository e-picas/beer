.\" man: Beer Manual
.\" man-name: beer
.\" author: humanity
.\" date: 0000-00-00
.\" version: 1.2.3
.TH  "beer" "3" "0000-00-00" "Version 1.2.3" "Beer Manual"
.SH NOM
.PP
Beer / Bière ("buy me a beer" - "it's beer time, right?")
.SH SYNOPSIS
.RS

.EX
      ``...............```                    
.br
   `-.``               ```....`               
.br
 `--.`` `````     `       ```.:`              
.br
 `:...```..:-``` ``  ```````..-:-..`          
.br
  ::::::::----.```````````....--....--        
.br
  -:--.``````.-/:..``.......--::.``  .:`      
.br
  .yo++-------/sss/-:++::so/:::///.` `.-      
.br
  `dyoo:------:ooso/syy++yydo-   :/-``./      
.br
  `hyoo:------:ooos+ssyooysh+    -+:.``/      
.br
  `yy++:------:ooosssssosssh/    -o:.`./      
.br
   yy++:-----::++++ooosysssh-    -o:.`./      
.br
   sy+/:------:++++++oosoooh-    :/:.`-/      
.br
   os//:-----::/+/++++oooooh.    +/-..:-      
.br
   os//:------://////+oooooy.   .+:-..+`      
.br
   ++/::-------::////+++o+os``-:/:-..:/       
.br
   +/::--------:::////+++++o+/:-...-/+`       
.br
   +/::--------::::://+++++o//:::/++-         
.br
  .+/::--------::::////++++osss+/-`           
.br
  /+++//::::::://///////+++os.                
.br
  ++/:::://////////++//////+s`                
.br
  +yso/::::---:::::::://+osyh`                
.br
   `.:::::::-------:::::::-.`                 
.br
            ````````                          
.EE
.RE
.SH DESCRIPTION
.PP
La bière est \fIune boisson alcoolisée\fP obtenue par fermentation de matières
glucidiques végétales et d'eau. Elle est obtenue par transformation de
matières amylacées par voies enzymatiques et microbiologiques.
.PP
Plus spécifiquement de nos jours, elle est généralement fabriquée à partir d’eau,
de malt (céréale germée, principalement de l'orge, parfois additionné d'autres céréales)
et depuis le Moyen Âge, de houblon. C'est l'une des boissons les plus populaires et
consommées au monde.
.PP
Remontant à l’Antiquité, la fabrication d'un produit fermenté à base de céréales
germées et appelé \fIsikaru\fP est attestée à Sumer au IVe millénaire av. J.-C..
.PP
Des versions très faiblement alcoolisées (variant de 2° à 0°) sont présentes
sur le marché. Contrairement aux autres boissons « sans alcool », elles sont
fabriquées par les mêmes procédés que la bière classique. La fabrication de la
bière est réalisée industriellement ou artisanalement dans une brasserie mais
est réalisable par le particulier.
.SH OÙ PUIS-JE EN BOIRE UNE?
.PP
Voici une liste des lieux vers Jaurès où vous pouvez boire une bière, dans quelles
conditions et à quels prix:
.SS 25° Est
.SS Côté Canal
.SS Péniche Antipode
.SS Franprix
.SH VOIR AUSSI
.PP
pub(1), happy-hour(1)
