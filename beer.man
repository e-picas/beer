.\" man: Beer Manual
.\" man-name: beer
.\" author: humanity
.\" date: 0000-00-00
.\" version: 1.2.3
.TH  "beer" "3" "0000-00-00" "Version 1.2.3" "Beer Manual"
.SH NAME
.PP
Beer ("buy me a beer" - "it's beer time, right?")
.SH SYNOPSIS
.RS

.EX
      ``...............```                    
.br
   `-.``               ```....`               
.br
 `--.`` `````     `       ```.:`              
.br
 `:...```..:-``` ``  ```````..-:-..`          
.br
  ::::::::----.```````````....--....--        
.br
  -:--.``````.-/:..``.......--::.``  .:`      
.br
  .yo++-------/sss/-:++::so/:::///.` `.-      
.br
  `dyoo:------:ooso/syy++yydo-   :/-``./      
.br
  `hyoo:------:ooos+ssyooysh+    -+:.``/      
.br
  `yy++:------:ooosssssosssh/    -o:.`./      
.br
   yy++:-----::++++ooosysssh-    -o:.`./      
.br
   sy+/:------:++++++oosoooh-    :/:.`-/      
.br
   os//:-----::/+/++++oooooh.    +/-..:-      
.br
   os//:------://////+oooooy.   .+:-..+`      
.br
   ++/::-------::////+++o+os``-:/:-..:/       
.br
   +/::--------:::////+++++o+/:-...-/+`       
.br
   +/::--------::::://+++++o//:::/++-         
.br
  .+/::--------::::////++++osss+/-`           
.br
  /+++//::::::://///////+++os.                
.br
  ++/:::://////////++//////+s`                
.br
  +yso/::::---:::::::://+osyh`                
.br
   `.:::::::-------:::::::-.`                 
.br
            ````````                          
.EE
.RE
.SH DESCRIPTION
.PP
The \fBbeer\fP command is a smart and very useful program that tells you if it
is time for a beer or not!
.PP
It basically analyzes the hour of the day and tells you if your have a lot
to wait before "beer-free" time or not ... Please note that it can be quite
hard to support someday, especially when you really need a beer at 9 am.
.PP
Beer is a program and must not be responsible of any alcoholism!!
.SH WHAT IS THAT?
.PP
Beer is an alcoholic beverage produced by the saccharification of starch and fermentation
of the resulting sugar. The starch and saccharification enzymes are often derived from malted
cereal grains, most commonly malted barley and malted wheat. Most beer is also flavoured
with hops, which add bitterness and act as a natural preservative, though other flavourings
such as herbs or fruit may occasionally be included. The brewing process causes a natural
carbonation effect (although forced carbonation is also used).
The preparation of beer is called brewing.
.PP
Beer is the world's most widely consumed alcoholic beverage, and is the third-most popular
drink overall, after water and tea. It is thought by some to be the oldest fermented beverage.
Beer is sold in bottles and cans and in pubs and bars it is available in draught form, which
uses large kegs of beer. Beer is typically served cold.
.PP
Some of humanity's earliest known writings refer to the production and distribution of beer:
the Code of Hammurabi included laws regulating beer and beer parlours, and "The Hymn to Ninkasi",
a prayer to the Mesopotamian goddess of beer, served as both a prayer and as a method of
remembering the recipe for beer in a culture with few literate people. Today, the brewing
industry is a global business, consisting of several dominant multinational companies and many
thousands of smaller producers ranging from brewpubs to regional breweries.
.SH WHERE CAN I DRINK ONE?
.PP
Below is a map of Jaurès' places where you can drink a beer with conditions and prices:
.SS 25° Est
.SS Côté Canal
.SS Péniche Antipode
.SS Franprix
.SH SEE ALSO
.PP
pub(1), happy-hour(1)
