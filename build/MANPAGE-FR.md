Man:        Beer Manual
Man-name:   beer
Author:     humanity
Date:       0000-00-00
Version:    1.2.3


## NOM

Beer / Bière ("buy me a beer" - "it's beer time, right?")

## SYNOPSIS

          ``...............```                    
       `-.``               ```....`               
     `--.`` `````     `       ```.:`              
     `:...```..:-``` ``  ```````..-:-..`          
      ::::::::----.```````````....--....--        
      -:--.``````.-/:..``.......--::.``  .:`      
      .yo++-------/sss/-:++::so/:::///.` `.-      
      `dyoo:------:ooso/syy++yydo-   :/-``./      
      `hyoo:------:ooos+ssyooysh+    -+:.``/      
      `yy++:------:ooosssssosssh/    -o:.`./      
       yy++:-----::++++ooosysssh-    -o:.`./      
       sy+/:------:++++++oosoooh-    :/:.`-/      
       os//:-----::/+/++++oooooh.    +/-..:-      
       os//:------://////+oooooy.   .+:-..+`      
       ++/::-------::////+++o+os``-:/:-..:/       
       +/::--------:::////+++++o+/:-...-/+`       
       +/::--------::::://+++++o//:::/++-         
      .+/::--------::::////++++osss+/-`           
      /+++//::::::://///////+++os.                
      ++/:::://////////++//////+s`                
      +yso/::::---:::::::://+osyh`                
       `.:::::::-------:::::::-.`                 
                ````````                          
                                                  

## DESCRIPTION

La bière est *une boisson alcoolisée* obtenue par fermentation de matières
glucidiques végétales et d'eau. Elle est obtenue par transformation de
matières amylacées par voies enzymatiques et microbiologiques.

Plus spécifiquement de nos jours, elle est généralement fabriquée à partir d’eau,
de malt (céréale germée, principalement de l'orge, parfois additionné d'autres céréales)
et depuis le Moyen Âge, de houblon. C'est l'une des boissons les plus populaires et
consommées au monde.

Remontant à l’Antiquité, la fabrication d'un produit fermenté à base de céréales
germées et appelé *sikaru* est attestée à Sumer au IVe millénaire av. J.-C..

Des versions très faiblement alcoolisées (variant de 2° à 0°) sont présentes
sur le marché. Contrairement aux autres boissons « sans alcool », elles sont
fabriquées par les mêmes procédés que la bière classique. La fabrication de la
bière est réalisée industriellement ou artisanalement dans une brasserie mais
est réalisable par le particulier.

## OÙ PUIS-JE EN BOIRE UNE?

Voici une liste des lieux vers Jaurès où vous pouvez boire une bière, dans quelles
conditions et à quels prix:

### 25° Est
### Côté Canal
### Péniche Antipode
### Franprix

## VOIR AUSSI

pub(1), happy-hour(1)
