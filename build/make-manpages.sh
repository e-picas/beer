#!/usr/bin/env bash

rm -rf ./../beer*.man
../../markdown-extended/bin/markdown-extended -f man -o ../beer.fr.man MANPAGE-FR.md
../../markdown-extended/bin/markdown-extended -f man -o ../beer.man MANPAGE-EN.md
man ./../beer.man
