#!/usr/bin/env bash

# ./beer [hour] [minute] [second] [day]

loopoveritself=true
thisday=$(date +'%w')
thishour=$(date +'%H')
thisminute=$(date +'%M')
thissecond=$(date +'%S')

[ $# -gt 0 ] && thishour="$1";
[ $# -gt 0 ] && loopoveritself=false;
[ $# -gt 1 ] && thisminute="$2";
[ $# -gt 2 ] && thissecond="$3";
[ $# -gt 3 ] && thisday="$4";

#echo "$thisday $thishour:$thisminute:$thissecond"

beer_big() {
    cat <<'MSG'

                         `..-------...---------...``                                                
                    `..--..``````````````````````..---....```                                       
                .---..````  `      `        `       `````...--.`                                    
              `:-.`````   `      `   ` `         `   `    ````.-----.`                              
            `-:-..````   ``  ` ` `     `    ``        `   `` ````...::`                             
           ./:...`````   ``````` ``  `       ` ```  ` `     ``````.../-                             
           :/-....`````````````..``````   ` ``     ```  ``````````...::`                            
           ./--......``......-://-.````  ` ``   ` ``   ````````......-//:::--.``                    
           `+:---------:::////////-.`````  ``  ````````````````.....-::------::::.`                 
            -+/:::////:::----....-:-..```````` ```````````.``.....---:-...`````..:/-`               
             //:::----.```````````..-:--.```````````.```..........---::...```   ``.:/.              
             /:------.``````````````.:///:-..`..`...............-----:/:-..``     `.:/`             
             /+//:::::..............-/oooss+:.....--------:::------::+o++/:.```    `.:/             
             /dhysoooo:--------------+ssssyyo/---/osso:::/yyys+::::/-::///+/-.```  ``-+`            
             :ddhsoooo:--------------/oosssss+::/yyhhh+/:+hhhhhhs/+:      .o/-..`````./-            
             .dhhsoooo:--------------/oooossso//+yyyyyo+/oyyyyhdm--`       +o:-..````.//            
             .dhhsoooo/--------------/oooossso+/ossyyyo++syysyhdd.         /o/:-.````.:/            
             .hhhyoooo/--------------/ooooosso++osssyyo++syssshdh`         /s/:-.````.:/`           
             `yhhyo+o+/--------------/oooooossoossssyso++yyssshdy`         /s+:-..```.:/`           
             `yhhy++++/--------------/ooooooossssoossssosysssshds`         /s+:-..```.:/`           
             `shhy++++/:-------------:+ooo+oooooooossssyyyssssydo`         /s+/:-.```.//            
              ohhy++++/:--------:--:-:+++++++o++ooosssyyyssosshd+          +s+/:-.```.//            
              ohhy++/+/:-------------:++++++++++++oossssssooosyd+          oo/:--.``.-+/            
              +hys++///:------:------:+++++++++++++ooooosoooooyd/         `s+/:-..``.-+-            
              +hys+////:-------------:+++++++++++++oooooooooooyd/         .s+/:-..``.:o`            
              /hyo/////-----------:--://++//+++++++oooooooooooyd:         /o/::-....-/o`            
              /hyo////:-------------:://////+/+/++++oooooooooosh:        `o+/::-....-+:             
              :hy+///:::----------:--:////+///////++oooooooooosh-        :o//:--...-:o-             
              :yy+//:::--------------:/://////////++o+ooooo+oosh-       .++/:--....-+o              
              -ys//::::---------------:///////////+++++++oo++ooy-   `.:///:---....-/s-              
              -yo//::::--------------::::::////////++++++++++oos:-:///::---......-:o+`              
              -s+//::::---------------:::::////////++++++++++ooso+//::--.......-:/oo`               
              :o//:::::---------------::::::://////+++++++++++oo+/:::----...--:/os+.                
              /+//:::::---------------::::::::::////++++++++++oo+/:::::--:://+os+-                  
             `++/:::::----------------:::::::::://///+++++++++ooo///////++oso+:.                    
             .o//:::::----------------:::::::::://////++++++++ooossossssso/-`                       
             ++///::::::::::::::::::::/:///////////////+++++++ooossso+:-`                           
            -oo++++//////::::::::::::://///////////////++++++++oos.`                                
            :so+o+++++///::::::::::::::::::::::::://////++++oooosy-                                 
            :o//:///////////////////////////////++++++++++++++++oy-                                 
            :ho+/::---..---::////////+++++++++/////::::::::://+osh-                                 
            :dyysoo+//::---------------------------:::://++oossyhd-                                 
            .oyyysso++////////::::::::////////////////+++oossyyhyo`                                 
              `.-:+o++//::--------.........--------::///+ooo+/:.`                                   
                    ``..--:::////:::::::::::::////::::-..``                                         
                             ````....----.....```

MSG
}

beer_medium() {
    cat <<'MSG'

          ``...............```                    
       `-.``               ```....`               
     `--.`` `````     `       ```.:`              
     `:...```..:-``` ``  ```````..-:-..`          
      ::::::::----.```````````....--....--        
      -:--.``````.-/:..``.......--::.``  .:`      
      .yo++-------/sss/-:++::so/:::///.` `.-      
      `dyoo:------:ooso/syy++yydo-   :/-``./      
      `hyoo:------:ooos+ssyooysh+    -+:.``/      
      `yy++:------:ooosssssosssh/    -o:.`./      
       yy++:-----::++++ooosysssh-    -o:.`./      
       sy+/:------:++++++oosoooh-    :/:.`-/      
       os//:-----::/+/++++oooooh.    +/-..:-      
       os//:------://////+oooooy.   .+:-..+`      
       ++/::-------::////+++o+os``-:/:-..:/       
       +/::--------:::////+++++o+/:-...-/+`       
       +/::--------::::://+++++o//:::/++-         
      .+/::--------::::////++++osss+/-`           
      /+++//::::::://///////+++os.                
      ++/:::://////////++//////+s`                
      +yso/::::---:::::::://+osyh`                
       `.:::::::-------:::::::-.`                 
                ````````                          
                                                  
MSG
}

beer_image() {
    local lines=$(tput lines)
    local beer
    if [ "$lines" -lt 50 ]; then
        beer="$(beer_medium)"
    else
        beer="$(beer_big)"
    fi
    bold "$beer" 
}

bold() {
    local bold=$(tput bold)
    local normal=$(tput sgr0)
    echo "${bold}${1}${normal}"
}

waiter() {
    local limithour="${1:-11}"
    local limitminute="${2:-00}"
    local waitminutes=$(($((60 - $((10#${limitminute})))) - $((10#${thisminute}))))
    local waithours=$(($((10#${limithour})) - $((10#${thishour}))))
    if [ $waitminutes -lt 0 ]; then
        waitminutes=$((60 + waitminutes))
        waithours=$((waithours - 1))
    fi
    local waitseconds=$((60 - $((10#${thissecond}))))
    local wait=''
    if [ "$waithours" -gt 0 ]; then
        wait="$waithours h $waitminutes minutes $waitseconds secondes"
    elif [ "$waitminutes" -gt 0 ]; then
        wait="$waitminutes minutes $waitseconds secondes"
    else
        wait="$waitseconds secondes"
    fi
    echo "patiente encore ... $(bold "$wait")"
}

what_image() {
    cat <<'MSG'
     ___ ___            _           _     ___ ___ 
    |__ \__ \ __      _| |__   __ _| |_  |__ \__ \
      / / / / \ \ /\ / / '_ \ / _` | __|   / / / /
     |_| |_|   \ V  V /| | | | (_| | |_   |_| |_| 
     (_) (_)    \_/\_/ |_| |_|\__,_|\__|  (_) (_) 

MSG
}

bientot_image() {
    cat <<'MSG'
     _     _            _        _   
    | |__ (_) ___ _ __ | |_ ___ | |_ 
    | '_ \| |/ _ \ '_ \| __/ _ \| __|
    | |_) | |  __/ | | | || (_) | |_ 
    |_.__/|_|\___|_| |_|\__\___/ \__|

MSG
}

yeah_image() {
    cat <<'MSG'
                      _       _ 
     _   _  ___  __ _| |__   | |
    | | | |/ _ \/ _` | '_ \  | |
    | |_| |  __/ (_| | | | | |_|
     \__, |\___|\__,_|_| |_| (_)
     |___/
                               
MSG
}

sure_image() {
    cat <<MSG
                          _ 
     ___ _   _ _ __ ___  | |
    / __| | | | '__/ _ \ | |
    \__ \ |_| | | |  __/ |_|
    |___/\__,_|_|  \___| (_)
                                                      
MSG
}


bientot_du_matin() {
    local wait="$(waiter 11 00)"
    bientot_image
    cat <<MSG
----------------------------------------------------------------------
$wait
----------------------------------------------------------------------
MSG
}

bientot_du_soir() {
    local wait="$(waiter 17 30)"
    bientot_image
    cat <<MSG
----------------------------------------------------------------------
$wait
----------------------------------------------------------------------
MSG
}

alcoolique_du_matin() {
    local wait="$(waiter 11 00)"
    local now="$thishour h $thisminute"
    local bold_now="$(bold "$now")"
    what_image
    cat <<MSG
----------------------------------------------------------------------
$bold_now c'est un peu tôt pour une bière non ? (!)
soit tu deviens alcoolique, soit tu te fais vraiment ch** .....
dans tous les cas, $wait
----------------------------------------------------------------------
MSG
}

alcoolique_du_soir() {
    local wait="$(waiter 17 30)"
    local now="$thishour h $thisminute"
    local bold_now="$(bold "$now")"
    what_image
    cat <<MSG
----------------------------------------------------------------------
$bold_now c'est un peu tôt pour une bière non ? (!)
fais une petite sieste, ça ira mieux ...
$wait
----------------------------------------------------------------------
MSG
}

daccord() {
    local yeah="$(yeah_image)"
    cat <<MSG
----------------------------------------------------------------------
$yeah
----------------------------------------------------------------------
MSG
    beer_image
}

daccord_week_end() {
    local sure="$(sure_image)"
    cat <<MSG
----------------------------------------------------------------------
$sure
----------------------------------------------------------------------
c'est le week-end, lache-toi !!
----------------------------------------------------------------------
MSG
    beer_image
}

################

$loopoveritself && clear;
#echo -e "\033[2J";

if [ "$thisday" -eq 0 ] || [ "$thisday" -eq 6 ]; then
    daccord_week_end

elif [ "$thishour" -lt 11 ]; then
    alcoolique_du_matin

elif [ "$thishour" -eq 11 ]; then
    bientot_du_matin

elif [ "$thishour" -gt 11 ] && [ "$thishour" -lt 14 ]; then
    daccord

elif [ "$thishour" -gt 13 ] && [ "$thishour" -lt 17 ]; then
    alcoolique_du_soir

elif [ "$thishour" -eq 17 ] && [ "$thisminute" -lt 30 ]; then
    bientot_du_soir

else
    daccord

fi

# loop over itself
$loopoveritself && sleep 5s && $0 $*;

